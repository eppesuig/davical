stages:
  - build
  - test
  - testall

build:
  stage: build
  image: debian:unstable
  script:
    - apt-get -y update
    - bash -c 'mkdir -p /usr/share/man/man{0..10}'
    - apt-get -y install build-essential devscripts fakeroot dh-exec jdupes doxygen graphviz php-cli phpunit
    - mv debian/changelog debian/changelog.old
    - >
      cat
      <(echo "davical ($(cat VERSION)-99~git$(date +"%Y%m%d")-$(git rev-parse --short $CI_COMMIT_SHA)) unstable; urgency=medium")
      <(echo "")
      <(echo "  * Build on CI")
      <(echo "")
      <(echo " -- ${GITLAB_USER_NAME} <${GITLAB_USER_EMAIL}>  $(date -R)")
      <(echo "")
      debian/changelog.old
      >debian/changelog
    - rm debian/changelog.old
    - curl 'https://gitlab.com/davical-project/awl/-/archive/master/awl-master.tar.gz' | tar zxf -
    - mv awl-master /usr/share/awl/
    - debuild -us -uc -b -d
    - 'mv ../davical_*_all.deb ./davical.deb'
  artifacts:
    paths:
      - '*.deb'

test:
  stage: test
  image: debian:unstable
  artifacts:
    paths:
      - testing/report.xml
      - apache2_log/*
      - davical_log/*
    reports:
      junit: testing/report.xml
    when:
      always
  script:
    - apt-get -y update
    - bash -c 'mkdir -p /usr/share/man/man{0..10}'
    - apt-get -y install locales
    - echo "en_NZ.UTF-8 UTF-8" >> /etc/locale.gen
    - locale-gen
    - echo "LANG=en_NZ.UTF-8" > /etc/default/locale
    - apt-get -y install libdbd-pg-perl libyaml-perl php php-cli php-pgsql php-xml postgresql-client postgresql libapache2-mod-php curl xmlstarlet
    - curl 'https://gitlab.com/davical-project/awl/-/archive/master/awl-master.tar.gz' | tar zxf -
    - mv awl-master /usr/share/awl/
    - chown -R www-data /usr/share/awl/
    - dpkg --ignore-depends=libawl-php -i *.deb
    - echo '127.0.1.1  regression mycaldav myempty' >> /etc/hosts
    - rm /etc/apache2/ports.conf /etc/apache2/sites-enabled/000-default.conf && touch /etc/apache2/ports.conf
    - cp testing/apache-site.conf.example /etc/apache2/sites-enabled/davical-regression.conf
    - sed -i 's/\/path\/to/\/usr\/share/g' /etc/apache2/sites-enabled/davical-regression.conf
    - mkdir /usr/share/davical/testing/
    - cp testing/*.php /usr/share/davical/testing/
    - rm /etc/davical/config.php
    - cat testing/regression-conf.php.example | sed 's.//$c->dbg.$c->dbg.' > /etc/davical/regression-conf.php
    - ln -s /etc/davical/regression-conf.php /etc/davical/mycaldav-conf.php
    - ln -s /etc/davical/regression-conf.php /etc/davical/myempty-conf.php
    - mkdir -p /var/log/davical
    - chown www-data /var/log/davical
    - sed -i '/peer/d' /etc/postgresql/15/main/pg_hba.conf
    - echo 'local  all  all  trust' >> /etc/postgresql/15/main/pg_hba.conf
    - pg_ctlcluster 15 main start
    - su postgres -c 'createuser davical_dba --createdb --createrole --superuser'
    - su postgres -c 'createuser davical_app --superuser'
    - su postgres -c 'createuser testrunner --superuser'
    - pg_ctlcluster 15 main restart
    - a2enmod rewrite
    - a2enmod headers
    - apache2ctl start
    - useradd testrunner
    - cd testing && su testrunner -c 'IS_CI=yes ALLSUITES="regression-suite binding carddav scheduling" ./run_regressions.sh all x'
  after_script:
    - mkdir -p apache2_log
    - cp -r /var/log/apache2 apache2_log/build
    - xz apache2_log/build/*
    - mkdir -p davical_log
    - cp -r /var/log/davical davical_log/build


test_bullseye_carddavclientinterop:
  stage: testall
  image: debian:bullseye
  artifacts:
    paths:
      - testing/report.xml
      - carddavclient-master/testreports/unit/results.html
      - apache2_log/*
      - davical_log/*
      - carddavclient-master/testreports/interop/*
    reports:
      junit:
        - testing/report.xml
        - carddavclient-master/testreports/unit/results.html
    when:
      always
  script:
    - apt-get -y update
    - bash -c 'mkdir -p /usr/share/man/man{0..10}'
    - apt-get -y install locales
    - echo "en_NZ.UTF-8 UTF-8" >> /etc/locale.gen
    - locale-gen
    - echo "LANG=en_NZ.UTF-8" > /etc/default/locale
    - apt-get -y install libdbd-pg-perl libyaml-perl php php-cli php-pgsql php-xml postgresql-client postgresql libapache2-mod-php curl xmlstarlet composer phpunit
    - curl 'https://gitlab.com/davical-project/awl/-/archive/master/awl-master.tar.gz' | tar zxf -
    - mv awl-master /usr/share/awl/
    - chown -R www-data /usr/share/awl/
    - dpkg --ignore-depends=libawl-php -i *.deb
    - echo '127.0.1.1  regression mycaldav myempty' >> /etc/hosts
    - rm /etc/apache2/ports.conf /etc/apache2/sites-enabled/000-default.conf && touch /etc/apache2/ports.conf
    - cp testing/apache-site.conf.example /etc/apache2/sites-enabled/davical-regression.conf
    - sed -i 's/\/path\/to/\/usr\/share/g' /etc/apache2/sites-enabled/davical-regression.conf
    - mkdir /usr/share/davical/testing/
    - cp testing/*.php /usr/share/davical/testing/
    - rm /etc/davical/config.php
    - cat testing/regression-conf.php.example | sed 's.//$c->dbg.$c->dbg.' > /etc/davical/regression-conf.php
    - ln -s /etc/davical/regression-conf.php /etc/davical/mycaldav-conf.php
    - ln -s /etc/davical/regression-conf.php /etc/davical/myempty-conf.php
    - mkdir -p /var/log/davical
    - chown www-data /var/log/davical
    - sed -i '/peer/d' /etc/postgresql/13/main/pg_hba.conf
    - echo 'local  all  all  trust' >> /etc/postgresql/13/main/pg_hba.conf
    - pg_ctlcluster 13 main start
    - su postgres -c 'createuser davical_dba --createdb --createrole --superuser'
    - su postgres -c 'createuser davical_app --superuser'
    - su postgres -c 'createuser testrunner --superuser'
    - pg_ctlcluster 13 main restart
    - a2enmod rewrite
    - a2enmod headers
    - apache2ctl start
    - useradd testrunner
    - cd testing && su testrunner -c 'IS_CI=yes ALLSUITES="regression-suite binding carddav scheduling" ./run_regressions.sh all x'
    - cd ..
    - curl https://codeload.github.com/mstilkerich/carddavclient/tar.gz/master | tar zxf -
    - cd carddavclient-master
    - composer install
    - echo '<?php declare(strict_types=1); namespace MStilkerich\Tests\CardDavClient\Interop; use MStilkerich\CardDavClient\{Account,AddressbookCollection,Config}; final class AccountData { public const ACCOUNTS = [ "Davical" => [ "username" => "user3", "password" => "user3", "discoveryUri" => "http://regression", "syncAllowExtraChanges" => false, "featureSet" => TestInfrastructureSrv::SRVFEATS_DAVICAL, ], ]; public const ADDRESSBOOKS = [ "Davical_0" => [ "account" => "Davical", "url" => "http://regression/caldav.php/user3/addresses/", "displayname" => "user3 addresses", "description" => null, ], ]; }' > tests/Interop/AccountData.php
    - mkdir -p testreports/interop
    - vendor/bin/phpunit -c tests/Interop/phpunit.xml --no-coverage
  after_script:
    - mkdir -p apache2_log
    - cp -r /var/log/apache2 apache2_log/test_bullseye_carddavclientinterop
    - bzip2 apache2_log/test_bullseye_carddavclientinterop/*
    - mkdir -p davical_log
    - cp -r /var/log/davical davical_log/test_bullseye_carddavclientinterop


build_bullseye_latestphp:
  stage: testall
  image: php:apache-bullseye
  script:
    - apt-get -y update
    - bash -c 'mkdir -p /usr/share/man/man{0..10}'
    - apt-get -y install build-essential devscripts fakeroot dh-exec jdupes doxygen graphviz
    - mv debian/changelog debian/changelog.old
    - >
      cat
      <(echo "davical ($(cat VERSION)-99~git$(date +"%Y%m%d")-$(git rev-parse --short $CI_COMMIT_SHA)) unstable; urgency=medium")
      <(echo "")
      <(echo "  * Build on CI")
      <(echo "")
      <(echo " -- ${GITLAB_USER_NAME} <${GITLAB_USER_EMAIL}>  $(date -R)")
      <(echo "")
      debian/changelog.old
      >debian/changelog
    - rm debian/changelog.old
    - curl 'https://gitlab.com/davical-project/awl/-/archive/master/awl-master.tar.gz' | tar zxf -
    - mv awl-master /usr/share/awl/
    - curl -o /usr/bin/phpunit -L https://phar.phpunit.de/phpunit-9.phar
    - chmod +x /usr/bin/phpunit
    - debuild --prepend-path=/usr/local/bin -us -uc -b -d
    - 'mv ../davical_*_all.deb ./davical.deb'
  artifacts:
    paths:
      - '*.deb'

test_bullseye_latestphp:
  stage: testall
  image: php:apache-bullseye
  artifacts:
    paths:
      - testing/report.xml
      - apache2_log/*
      - davical_log/*
    reports:
      junit: testing/report.xml
    when:
      always
  script:
    - apt-get -y update
    - bash -c 'mkdir -p /usr/share/man/man{0..10}'
    - apt-get -y install locales
    - echo "en_NZ.UTF-8 UTF-8" >> /etc/locale.gen
    - locale-gen
    - echo "LANG=en_NZ.UTF-8" > /etc/default/locale
    - apt-get -y install libdbd-pg-perl libyaml-perl perl postgresql postgresql-client libpq-dev xmlstarlet
    - curl 'https://gitlab.com/davical-project/awl/-/archive/master/awl-master.tar.gz' | tar zxf -
    - mv awl-master /usr/share/awl/
    - chown -R www-data /usr/share/awl/
    - dpkg --ignore-depends=php,php-pgsql,php-xml,libawl-php,php-cli -i *.deb
    - docker-php-ext-install -j$(nproc) pgsql
    - docker-php-ext-install -j$(nproc) pdo_pgsql
    - docker-php-ext-install -j$(nproc) calendar
    - echo '127.0.1.1  regression mycaldav myempty' >> /etc/hosts
    - rm /etc/apache2/ports.conf /etc/apache2/sites-enabled/000-default.conf && touch /etc/apache2/ports.conf
    - cp testing/apache-site.conf.example /etc/apache2/sites-enabled/davical-regression.conf
    - sed -i 's/\/path\/to/\/usr\/share/g' /etc/apache2/sites-enabled/davical-regression.conf
    - mkdir /usr/share/davical/testing/
    - cp testing/*.php /usr/share/davical/testing/
    - rm /etc/davical/config.php
    - cat testing/regression-conf.php.example | sed 's.//$c->dbg.$c->dbg.' > /etc/davical/regression-conf.php
    - ln -s /etc/davical/regression-conf.php /etc/davical/mycaldav-conf.php
    - ln -s /etc/davical/regression-conf.php /etc/davical/myempty-conf.php
    - mkdir -p /var/log/davical
    - chown www-data /var/log/davical
    - sed -i '/peer/d' /etc/postgresql/13/main/pg_hba.conf
    - echo 'local  all  all  trust' >> /etc/postgresql/13/main/pg_hba.conf
    - pg_ctlcluster 13 main start
    - su postgres -c 'createuser davical_dba --createdb --createrole --superuser'
    - su postgres -c 'createuser davical_app --superuser'
    - su postgres -c 'createuser testrunner --superuser'
    - pg_ctlcluster 13 main restart
    - a2enmod rewrite
    - a2enmod headers
    - a2dismod -f deflate
    - apache2ctl start
    - useradd testrunner
    - cd testing && su testrunner -c 'IS_CI=yes ALLSUITES="regression-suite binding carddav scheduling" ./run_regressions.sh all x'
  after_script:
    - mkdir -p apache2_log
    - cp -r /var/log/apache2 apache2_log/test_bullseye_latestphp
    - bzip2 apache2_log/test_bullseye_latestphp/* || true
    - mkdir -p davical_log
    - cp -r /var/log/davical davical_log/test_bullseye_latestphp

test_memcache:
  stage: testall
  image: debian:unstable
  artifacts:
    paths:
      - testing/report.xml
      - apache2_log/*
      - davical_log/*
    reports:
      junit: testing/report.xml
    when:
      always
  script:
    - apt-get -y update
    - bash -c 'mkdir -p /usr/share/man/man{0..10}'
    - apt-get -y install locales
    - echo "en_NZ.UTF-8 UTF-8" >> /etc/locale.gen
    - locale-gen
    - echo "LANG=en_NZ.UTF-8" > /etc/default/locale
    - apt-get -y install libdbd-pg-perl libyaml-perl php php-cli php-pgsql php-xml php-memcached postgresql-client postgresql libapache2-mod-php curl xmlstarlet memcached
    - phpenmod memcached
    - curl 'https://gitlab.com/davical-project/awl/-/archive/master/awl-master.tar.gz' | tar zxf -
    - mv awl-master /usr/share/awl/
    - chown -R www-data /usr/share/awl/
    - dpkg --ignore-depends=libawl-php -i *.deb
    - echo '127.0.1.1  regression mycaldav myempty' >> /etc/hosts
    - rm /etc/apache2/ports.conf /etc/apache2/sites-enabled/000-default.conf && touch /etc/apache2/ports.conf
    - cp testing/apache-site.conf.example /etc/apache2/sites-enabled/davical-regression.conf
    - sed -i 's/\/path\/to/\/usr\/share/g' /etc/apache2/sites-enabled/davical-regression.conf
    - mkdir /usr/share/davical/testing/
    - cp testing/*.php /usr/share/davical/testing/
    - rm /etc/davical/config.php
    - cat testing/regression-conf.php.example | sed 's.//$c->dbg.$c->dbg.' | sed 's.//memcache ..g' > /etc/davical/regression-conf.php
    - ln -s /etc/davical/regression-conf.php /etc/davical/mycaldav-conf.php
    - ln -s /etc/davical/regression-conf.php /etc/davical/myempty-conf.php
    - mkdir -p /var/log/davical
    - chown www-data /var/log/davical
    - sed -i '/peer/d' /etc/postgresql/15/main/pg_hba.conf
    - echo 'local  all  all  trust' >> /etc/postgresql/15/main/pg_hba.conf
    - pg_ctlcluster 15 main start
    - su postgres -c 'createuser davical_dba --createdb --createrole --superuser'
    - su postgres -c 'createuser davical_app --superuser'
    - su postgres -c 'createuser testrunner --superuser'
    - pg_ctlcluster 15 main restart
    - a2enmod rewrite
    - a2enmod headers
    - apache2ctl start
    - /etc/init.d/memcached start
    - useradd testrunner
    # testrunner needs to be able to read /var/log/apache2/regression-error.log for the memcache tests.
    - adduser testrunner adm
    - cd testing && su testrunner -c 'IS_CI=yes ALLSUITES="regression-suite binding carddav scheduling" ./run_regressions.sh all x'
  after_script:
    - mkdir -p apache2_log
    - cp -r /var/log/apache2 apache2_log/test_memcache
    - xz apache2_log/test_memcache/*
    - mkdir -p davical_log
    - cp -r /var/log/davical davical_log/test_memcache
