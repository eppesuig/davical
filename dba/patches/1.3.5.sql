
-- Notable enhancement:	add shadow columns to calendar_item

BEGIN;
SELECT check_db_revision(1,3,4);

ALTER TABLE calendar_item
  ADD COLUMN dtstart_orig TIMESTAMP WITH TIME ZONE,
  ADD COLUMN dtend_orig TIMESTAMP WITH TIME ZONE;

-- We don't know what the user sent us without reparsing all the VCALENDAR
-- blobs, which I'm not going to do in SQL. We'll just have to wing it, and
-- hope this is good enough (it is no better than the current situation for
-- existing deployments) and dtstart_orig and dtend_orig will end up with
-- correct data over time as users re-upload the VCALENDAR blobs.
UPDATE calendar_item
SET
  dtstart_orig = dtstart,
  dtend_orig = dtend;

-- http://blogs.transparent.com/polish/names-of-the-months-and-their-meaning/
SELECT new_db_revision(1,3,5, 'Maj' );

COMMIT;
ROLLBACK;
