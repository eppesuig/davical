const privilege_types  = ['grant_privileges', 'ticket_privileges', 'default_privileges'];

for (const fname of privilege_types) {
    let element = document.getElementById("toggle_priv_"+fname+"_all");
    if (element != null ) {
      element.addEventListener('click', function() {
      toggle_privileges(fname, 'all');
      });
    }
    element = document.getElementById("toggle_priv_"+fname+"_rw");
    if (element != null ) {
      element.addEventListener('click', function() {
      toggle_privileges(fname, 'read', 'write-properties', 'write-content', 'bind', 'unbind', 'read-free-busy', 'read-current-user-privilege-set',
                        'schedule-deliver-invite', 'schedule-deliver-reply', 'schedule-query-freebusy', 'schedule-send-invite', 'schedule-send-reply',
                        'schedule-send-freebusy' );
      });
    }
    element = document.getElementById("toggle_priv_"+fname+"_read");
    if (element != null ) {
      element.addEventListener('click', function() {
        toggle_privileges(fname, 'read', 'read-free-busy', 'schedule-query-freebusy', 'read-current-user-privilege-set' );
      });
    }
    element = document.getElementById("toggle_priv_"+fname+"_fb");
    if (element != null ) {
      element.addEventListener('click', function() {
        toggle_privileges(fname, 'read-free-busy', 'schedule-query-freebusy' );
      });
    }
    element = document.getElementById("toggle_priv_"+fname+"_sd");
    if (element != null ) {
      element.addEventListener('click', function() {
        toggle_privileges(fname, 'schedule-deliver-invite', 'schedule-deliver-reply', 'schedule-query-freebusy' );
      });
    }
    element = document.getElementById("toggle_priv_"+fname+"_ss");
    if (element != null ) {
      element.addEventListener('click', function() {
        toggle_privileges(fname, 'schedule-send-invite', 'schedule-send-reply', 'schedule-send-freebusy' );
      });
    }
}
